#!/bin/bash

# This script is configured for a raspberry pi, which has user pi
# Make it run in /etc/rc.local to automatically launch the bot in a tmux session named pg-bot
# when the raspberry pi boots up

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
tmux new-session -d -s pg-bot "$DIR/anti-crash.sh"
