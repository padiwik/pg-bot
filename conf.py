# example config file

prefix =            # what commands start with
description =       # a description shown when giving the command help. also the useragent for praw

reddit_id =         # reddit script id
reddit_secret =     # reddit script secret

reddit_username =   # reddit username
reddit_pwd =        # reddit password

discord_token =     # discord secret token

chanID =            # discord id of main channel to send messages
guessChanID =       # discord id of channel to send guessses and replies to guesses

announceRounds =    # whether or not bot should announce new rounds
updateReddit =      # whether or not to continually fetch new round data, which can cause timeouts
askForInput =       # whether or not to ask for a value if it comes up as deleted/unknown

dataFile =          # filename where csv round data should be stored

