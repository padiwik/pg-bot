#!/bin/bash

DIR="$(cd "$(dirname "$0")" && pwd)"

while true
do
    sleep 1
    python3.7 $DIR/main.py config
done
