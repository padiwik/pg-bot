import datetime
import importlib
import logging
import re
import sys

import praw

import constants
import utils

config = importlib.import_module(sys.argv[1])

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

reddit = praw.Reddit(
    client_id=config.reddit_id,
    client_secret=config.reddit_secret,
    user_agent=config.description,
    username=config.reddit_username,
    password=config.reddit_pwd,
)
subreddit = reddit.subreddit("PictureGame")
mods = subreddit.moderator()
bots = constants.bots

data = utils.getAllData()

lastNum = -1

prevCorrectTime = utils.getCorrectTime(int(sys.argv[2]) - 1)

for n in range(int(sys.argv[2]), int(sys.argv[3])):
    if n > 1:
        badRound = False
        print(n)
        searcher = subreddit.search(
            f'"[Round {n}]" subreddit:PictureGame', limit=5, syntax="lucene", sort="old"
        )
        l = list(searcher)
        if len(l) != 1:
            x = input(f"Give me a round link [I found {len(l)}] ")
            if x == "skip":
                continue
            else:
                currentRound = reddit.submission(url=x)
                badRound = True
        else:
            currentRound = l[0]
        print(currentRound.title)
        if currentRound.link_flair_text != "ROUND OVER":
            print(currentRound.link_flair_text)
        else:
            currentRound.comment_sort = "new"  # by old, no?
            currentComments = currentRound.comments
            currentComments.replace_more(limit=0)
            if lastNum != n - 1:
                prevCorrectTime = utils.getCorrectTime(n - 1)
            correctComment = ""
            winner = ""
            poster = currentRound.author.name
            for comment in currentComments.list():
                # this code does not handle multiple +corrects properly
                if comment.author and comment.author.name in bots:
                    if comment.is_root:
                        if "punishable  offence" in comment.body:
                            found = re.findall("#\*(.*)\*:", comment.body)
                            poster = found[1].replace("\\", "")
                        if "winning this round!" in comment.body:
                            found = re.findall("to (.*) on", comment.body)
                            winner = found[0].replace("\\", "")
                if (
                    utils.isValidOpComment(comment, poster, mods, bots) or badRound
                ) and utils.isValidCorrectorComment(
                    comment, bots + [poster or currentRound.author.name]
                ):
                    correctComment = comment
            if correctComment:
                s = utils.writeData(
                    currentRound,
                    correctComment,
                    n,
                    prevCorrectTime,
                    poster=poster,
                    winner=winner,
                )
                prevCorrectTime = s["cTimeStamp"]
                print("time", prevCorrectTime)
                lastNum = n
