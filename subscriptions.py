# This file holds the setup code and discord commands for subscriptions

import asyncio
import discord
from discord.ext import commands
import shelve
import discord as discordpy
import shlex

import constants
import reddit
import utils


class SubscriptionType:
    def __init__(self, name, fullDescription, subKeywords):
        self.subKeywords = subKeywords
        self.shortDescription = subKeywords[0]  # e.g. "guesses"
        self.fullDescription = fullDescription  # e.g. "guesses on rounds by"
        self.name = name
        self.data = None


subscriptionTypes = (
    SubscriptionType("guessPingees", "guesses on rounds by", ("guesses",)),
    SubscriptionType("hintPingees", "hints on rounds by", ("hints",)),
    SubscriptionType("replyPingees", "replies to guesses by", ("replies",)),
    SubscriptionType("winPingees", "wins by", ("wins", "winner",)),
    SubscriptionType(
        "roundPingees", "rounds by", ("rounds", "posts", "post", "host", "author",)
    ),
    SubscriptionType(
        "titlePingees",
        "rounds whose title contains any of",
        ("titles", "title", "substring", "substrings"),
    ),
)

# Don't parse common prepositions as usernames
ignoreList = ("by", "from", "to", "on", "containing", "contains")


def getSubscriptionsPingees():
    """Returns subscriptions as a tuple of the data stored in each SubscriptionType"""

    # We load from the shelf because I can't figure out how to use the updated dictionary in-memory
    # when someone subscribes to something

    # Pre-populate subscriptions data from shelf, or create template if none exists
    with shelve.open(constants.shelfName) as shelf:
        for subscriptionType in subscriptionTypes:
            if not subscriptionType.name in shelf:
                shelf[subscriptionType.name] = constants.pingeesFormat.copy()
            subscriptionType.data = shelf[subscriptionType.name]

    return map(lambda x: x.data, subscriptionTypes)


getSubscriptionsPingees()


def save_subscriptions():
    with shelve.open(constants.shelfName) as shelf:
        for subscriptionType in subscriptionTypes:
            shelf[subscriptionType.name] = subscriptionType.data
    shelf.close()


class SubscriptionsCog(commands.Cog, name="Subscriptions"):
    def __init__(self, bot):
        self.bot = bot
        self.reddit = reddit

    subscribeUsage = "[subType='guesses'] all | [user] | round [number ...] ..."

    @commands.command(aliases=["subscribe", "sub"], usage=subscribeUsage)
    async def pingSubscribe(self, ctx, *, args: str = ""):
        "Subscribe to mentions for guesses, hints, guess replies, or wins."
        await self.parseSubscription(ctx, args, True)

    @commands.command(aliases=["unsub"], usage=subscribeUsage)
    async def unsubscribe(self, ctx, *, args: str = ""):
        "Unsubscribe from mentions for guesses, hints, guess replies, or wins."
        await self.parseSubscription(ctx, args, False)

    async def parseSubscription(self, ctx, args, isPositive: bool):
        async def autocomplete():
            if isRound and subsDict["round sub"]:
                return
            if not isRound and subsDict["all"] or subsDict["user sub"]:
                return
            if isRound:
                word = reddit.RedditParser.currentNum
            else:  # TODO don't autocomplete title subs
                word = user.display_name
            await self.handleSubscription(
                ctx,
                userId,
                selectedSubscriptionType,
                word,
                subsDict,
                isRound,
                isPositive,
            )

        user = ctx.message.author
        userId = user.id
        selectedSubscriptionType = subscriptionTypes[0]  # Parse guesses by default
        isRound = False
        subsDict = {"all": False, "user sub": [], "round sub": []}
        splitArgs = shlex.split(args)
        # Handle inverted order for backward compatibility
        if len(splitArgs) == 2 and splitArgs[1] in [
            "guesses",
            "hints",
            "replies",
            "wins",
        ]:
            splitArgs = [splitArgs[1], splitArgs[0]]
        firstArg = True
        for word in splitArgs:
            subTypeFound = False
            for subscriptionType in subscriptionTypes:
                if word in subscriptionType.subKeywords:
                    if not firstArg:
                        # don't parse any guesses in a scenario like .sub hints all
                        await autocomplete()
                        await self.printNewSubscription(
                            ctx, selectedSubscriptionType, subsDict, isPositive
                        )
                    selectedSubscriptionType = subscriptionType
                    subTypeFound = True
                    isRound = False
                    subsDict = {"all": False, "user sub": [], "round sub": []}
                    break
            if not subTypeFound:
                # This is a name or round number, etc
                if word in ignoreList:
                    continue
                if word == "round" or word == "rounds":
                    isRound = True
                else:
                    if isRound and not word.isdecimal():
                        await autocomplete()
                        isRound = False
                    await self.handleSubscription(
                        ctx,
                        userId,
                        selectedSubscriptionType,
                        word,
                        subsDict,
                        isRound,
                        isPositive,
                    )
            firstArg = False
        await autocomplete()
        await self.printNewSubscription(
            ctx, selectedSubscriptionType, subsDict, isPositive
        )
        asyncio.get_event_loop().run_in_executor(None, save_subscriptions)

    # called when we are done with a subscription type and need to print it to the user
    async def printNewSubscription(self, ctx, subscriptionType, subsDict, isPositive):
        msg = (
            "You will now be pinged for "
            if isPositive
            else "You will no longer be pinged for "
        )
        # here subsDict is sent as if everything is positive, even if it's an unsub
        msg += await self.printSubscription(subscriptionType, subsDict)
        await ctx.send(msg)

    async def printSubscription(self, subscriptionType, subsDict):
        msg = ""
        mainList = []
        roundsSet = subsDict["round sub"]
        usersSet = subsDict["user sub"]
        print(subsDict)
        if subsDict["all"]:
            mainList.append(f"all {subscriptionType.shortDescription}")
        if roundsSet:
            if len(roundsSet) == 1:
                mainList.append(
                    f"{subscriptionType.shortDescription} on round {min(roundsSet)}"
                )
            else:
                roundsText = ", ".join({str(round) for round in roundsSet})
                mainList.append(
                    f"{subscriptionType.shortDescription} on rounds {roundsText}"
                )
        if usersSet:
            usersText = ", ".join(usersSet)
            mainList.append(f"{subscriptionType.fullDescription} {usersText}")
        msg += ", ".join(mainList)
        roundsListExcept = subsDict.get("round unsub")
        usersListExcept = subsDict.get("user unsub")
        exceptList = []
        if roundsListExcept or usersListExcept:
            if not msg:
                msg = subscriptionType.shortDescription
            msg += " except "
            if roundsListExcept:
                if len(roundsListExcept) == 1:
                    exceptList.append(
                        f"{subscriptionType.shortDescription} on round {min(roundsListExcept)}"
                    )
                else:
                    roundsText = ", ".join({str(round) for round in roundsListExcept})
                    exceptList.append(
                        f"{subscriptionType.shortDescription} on rounds {roundsText}"
                    )
            if usersListExcept:
                usersText = ", ".join(usersListExcept)
                exceptList.append(f"{subscriptionType.fullDescription} {usersText}")
            msg += " and ".join(exceptList)
        return msg

    # called when we want to subscribe or unsubscribe from word
    async def handleSubscription(
        self, ctx, userId, subscriptionType, word, subsDict, isRound, isPositive
    ):
        pingeesDict = subscriptionType.data
        if word == "all":
            subsDict["all"] = True
            pingeesSub = pingeesDict["all"]
            pingeesUnsub = set()
        elif isRound:
            word = int(word)
            subsDict["round sub"].append(word)
            pingeesSub = pingeesDict["round sub"][word]
            pingeesUnsub = pingeesDict["round unsub"][word]
        else:
            word = word.lower()
            subsDict["user sub"].append(word)
            pingeesSub = pingeesDict["user sub"][word]
            pingeesUnsub = pingeesDict["user unsub"][word]
        if isPositive:
            if userId in pingeesSub:
                await ctx.send("Warning: You are already subscribed to this.")
            elif userId in pingeesUnsub:
                pingeesUnsub.discard(userId)
            else:
                pingeesSub.add(userId)
        else:
            if userId in pingeesUnsub:
                await ctx.send("Warning: You are already unsubscribed from this.")
            elif userId in pingeesSub:
                pingeesSub.discard(userId)
                # add output for all: "run this again to actually remove all subs"
            else:
                if word == "all":
                    await ctx.send(
                        "Warning: Not implemented yet, unsub manually from all your subscriptions"
                    )
                else:
                    pingeesUnsub.add(userId)

    @commands.command(aliases=["subs"])
    async def subscriptions(self, ctx, user: discordpy.User = None):
        "List all active subscriptions"
        if user is None:
            user = ctx.message.author
            msg = "You are subscribed to:"
        else:
            msg = f"{user.display_name} is subscribed to:"
        personId = user.id
        hasSubscriptions = False
        for subscriptionType in subscriptionTypes:
            pingeesDict = subscriptionType.data
            subsDict = {"all": (personId in pingeesDict["all"])}
            for subtype in ["round sub", "round unsub", "user sub", "user unsub"]:
                subsDict[subtype] = {
                    key for key, val in pingeesDict[subtype].items() if personId in val
                }
            prettyList = await self.printSubscription(subscriptionType, subsDict)
            if not prettyList:
                # there are no subscriptions of this type
                continue
            else:
                hasSubscriptions = True
            msg += f"\n{prettyList}"
        if not hasSubscriptions:
            # remove the final colon
            msg = msg[:-1] + " nothing"
        print(msg)
        await ctx.send(msg)

    @commands.command()
    @commands.is_owner()
    async def cleanSubs(self, ctx):
        clean_old()

    @subscriptions.error
    async def subscriptions_error(self, ctx, error):
        if isinstance(error, commands.BadArgument):
            await ctx.send("Error: Could not find this user")


def mentionsSuffix(
    discord,
    mentionsDict,
    username,
    roundNum,
    excludeName="",
    substringMentionsDict=None,
    stringToSearch=None,
):
    pingeeIds = mentionsDict["round sub"][roundNum].copy()
    if substringMentionsDict and stringToSearch:
        # First, add pingees who subscribe to a substring which is contained in stringToSearch (e.g. a round title)
        normalizedString = stringToSearch.lower()
        for substringSearch, substringPingeeIds in substringMentionsDict[
            "user sub"
        ].items():
            if substringSearch in normalizedString:
                pingeeIds.update(substringPingeeIds)
        # Remove pingees if they explicitly unsubscribe from a substring, which should be more specific than the above
        for substringSearch, substringPingeeIds in substringMentionsDict[
            "user unsub"
        ].items():
            if substringSearch in normalizedString:
                pingeeIds.difference_update(substringPingeeIds)
    for pingeeId in mentionsDict["user sub"][username.lower()]:
        if pingeeId not in mentionsDict["round unsub"][roundNum]:
            pingeeIds.add(pingeeId)
    for pingeeId in mentionsDict["all"]:
        if (
            pingeeId not in mentionsDict["user unsub"][username.lower()]
            and pingeeId not in mentionsDict["round unsub"][roundNum]
        ):
            pingeeIds.add(pingeeId)
    server = utils.getDiscordServer(discord)
    pingeeUsers = [server.get_member(user_id) for user_id in pingeeIds]
    mentions = [
        pingee.mention
        for pingee in pingeeUsers
        if pingee and pingee.display_name.lower() != excludeName.lower()
    ]
    return " " + ", ".join(mentions) if mentions else ""


# Delete subscriptions from old round numbers
def clean_old():
    for subscriptionType in subscriptionTypes:
        pingeesDict = subscriptionType.data
        for roundsDict in (pingeesDict["round sub"], pingeesDict["round unsub"]):
            for key in list(roundsDict.keys()):
                if key < reddit.RedditParser.currentNum:
                    del roundsDict[key]


def setup(bot):
    bot.add_cog(SubscriptionsCog(bot))
