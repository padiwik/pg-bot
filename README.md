# PG-Bot

This is a bot that helps with r/PictureGame on reddit and discord, made by u/padiwik

## How to run

Execute ```python3.7 main.py conf``` from the pg-bot directory

The bot is currently running at http://discord.gg/bTy2yA4

## License

CC-BY-SA 4.0 https://creativecommons.org/licenses/by-sa/4.0/
