#!/usr/bin/python3.7

import argparse
import asyncio
import csv
import datetime
import importlib
import logging
import math
import os
import shlex
import signal
import sys
import traceback
from collections import defaultdict

import requests
from discord.ext import commands

import constants
import reddit
import utils
import subscriptions


config = importlib.import_module(sys.argv[1])

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

discord = commands.Bot(command_prefix=config.prefix, description=config.description)
reddit = reddit.RedditParser(discord)


@discord.event
async def on_ready():
    print(f"{discord.user.name} {discord.user.id}")
    print("PG-Bot, at your service!")


@discord.command(hidden=True)
async def ping(ctx):
    await ctx.send(utils.pasteLongData("pong"))


@discord.command()
async def current(ctx):
    "Tells you the current PG round"
    await ctx.send(reddit.printCurrentPretty())


@discord.command()
async def roundsList(ctx):
    "Sends you to r/PictureGame/new"
    await ctx.send(constants.roundsList)


@discord.command()
async def time(ctx):
    "Prints how long since round was posted or a user +corrected"
    await ctx.send(reddit.printCurrentTime())


@discord.command(aliases=["lb", "l", "top"])
async def leaderboard(ctx, *, args: str = ""):
    "Gives leaderboard data for rounds following certain parameters"
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--maxReturns", "--count", default=50, type=int)
    parser.add_argument("-p", "--print", nargs="+")
    parser.add_argument("-f", "--filter", nargs="+")
    parser.add_argument("-r", "--raw", action="store_true")
    parser.add_argument("-n", "--unnumbered", action="store_true")
    keywordIndex = 1  # we are using api by default
    args, _ = parser.parse_known_args(shlex.split(args))
    maxReturns = args.maxReturns
    if maxReturns == 0:
        maxReturns = 999999  # reasonably large enough
    filterDict = utils.processFilter(args.filter, "winnerName", keywordIndex)
    filterString = utils.createFilterString(filterDict)
    payload = {
        "includeRoundNumbers": "false",
        "count": maxReturns,
        "filterRounds": filterString,
    }
    future = asyncio.get_event_loop().run_in_executor(
        None,
        lambda: requests.get("https://api.picturegame.co/leaderboard", params=payload),
    )
    r = await future
    print(r.url)
    message = ""
    for player in r.json()["leaderboard"]:
        if not args.unnumbered:
            message += f"{player['rank']}. "
        message += f"{player['username']}: {player['numWins']}\n"
    if len(message) > 2000:
        message = utils.pasteLongData(message)
    await ctx.send(message or "no matches")


sortUsage = "<toSort> <sortOrder> [sortBy] [-f | --filter <filter keywords>] [-p | --print <attributes to print>] [-m | --maxReturns <maximum returns>] [-n | --numbered] [-r | --raw] [--noapi]"
sortLongHelp = f"""Sample query: {config.prefix}sort winner asc winDelay --filter host quatrevingtneuf winDelay max 60 --print roundNumber --maxReturns 10

* Sort round winners in ascending order of the round duration
* Filter results to use only the rounds hosted by quatrevingtneuf and which lasted at most 60 seconds
* Print the round number for each result
* Return at most 10 users

Sample query: {config.prefix}sort host desc count --filter substring pa winDelay min 3600 --maxReturns 0 --numbered

* Sort round hosts in descending order of the number of times they appear
* Filter results to use only values we sort (hosts in this case) which contain the substring "pa" and are rounds which lasted at least 3600 seconds (1 hour)
* No limit on the maximum number of users to return
* Number the results like a leaderboard"""


@discord.command(usage=sortUsage, help=sortLongHelp)
async def sort(ctx, toSort: str, sortOrder: str, *, args: str = ""):
    "Sorts a parameter based on values in specific rounds."
    parser = argparse.ArgumentParser()
    parser.add_argument("sortKey", nargs="?")  # sort BY, can be "count"
    parser.add_argument("sortType", default="absolute", nargs="?")
    parser.add_argument("--limit", default=0, type=int)  # ceiling/floor for toSort
    parser.add_argument("-m", "--maxReturns", default=20, type=int)
    parser.add_argument("-p", "--print", nargs="+")
    parser.add_argument("-f", "--filter", nargs="+")
    parser.add_argument("-r", "--raw", action="store_true")
    parser.add_argument("--noapi", action="store_true")
    parser.add_argument("-n", "--numbered", action="store_true")
    args, _ = parser.parse_known_args(shlex.split(args))
    usingApi = not args.noapi
    keywordIndex = 1 if usingApi else 0
    if toSort not in constants.keywords.keys():
        message = f"Error: {toSort} is not a valid parameter."
        await ctx.send(message)
        return
    sortKey = args.sortKey
    if sortKey != "count":
        if sortKey not in list(constants.keywords.keys()):
            sortKey = toSort
        sortKey = constants.keywords[sortKey][keywordIndex]
    toSort = constants.keywords[toSort][keywordIndex]
    sortType = args.sortType
    limit = args.limit
    maxReturns = args.maxReturns
    if maxReturns == 0:
        maxReturns = math.inf
    toReturn = (
        [constants.keywords[elem][keywordIndex] for elem in args.print]
        if args.print
        else []
    )
    if sortKey not in toReturn:
        toReturn = [sortKey] + toReturn
    if usingApi and toSort not in toReturn:
        toFetch = [toSort] + toReturn
    else:
        toFetch = toReturn
    print(toReturn)
    index = toReturn.index(sortKey)
    filterDict = utils.processFilter(args.filter, toSort, keywordIndex)
    if sortOrder in ["max", "desc", "descending"]:
        sortOrder = ["max", "desc"][keywordIndex]
    elif sortOrder in ["min", "asc", "ascending"]:
        sortOrder = ["min", "asc"][keywordIndex]
    if usingApi:
        filterString = utils.createFilterString(filterDict)
        if sortKey == "count":
            payload = {
                "groupBy": toSort,
                "groupSort": "numRounds " + sortOrder,
                "filter": filterString,
            }
            future = asyncio.get_event_loop().run_in_executor(
                None,
                lambda: requests.get(
                    f"{constants.API_BASE_URL}rounds/aggregate", params=payload
                ),
            )
            r = await future
            print(r.url)
            message = ""
            count = 1
            for line in r.json()["results"]:
                if count > maxReturns:
                    break
                if args.numbered:
                    message += f"{count}. "
                message += (
                    f"{utils.getKey(line, toSort, args.raw)}: {line['numRounds']}\n"
                )
                count += 1
            # TODO: multiple pages
        else:
            offset = 0
            values = defaultdict(list)
            isReversed = sortOrder == "desc"
            while True:
                payload = {
                    "filter": filterString,
                    "sort": sortKey + " " + sortOrder,
                    "select": ",".join(toFetch),
                }
                if offset > 0:
                    payload["offset"] = offset
                future = asyncio.get_event_loop().run_in_executor(
                    None,
                    lambda: requests.get(
                        f"{constants.API_BASE_URL}rounds", params=payload
                    ),
                )
                r = await future
                print(r.url)
                message = ""
                r = r.json()
                for line in r["results"]:
                    # maxReturns should be implemented in the api request if possible
                    x = line.get(sortKey)
                    if x:
                        if isinstance(x, str) and x.isdigit():
                            x = int(x)
                        if (
                            x > 0
                            and (
                                utils.inLimit(x, limit, isReversed) or sortType == "avg"
                            )
                            and line.get(toSort)
                        ):
                            # we need a ternary here because x is recast to int
                            values[line[toSort]].append(
                                [
                                    (x if y == sortKey else line.get(y, "?"))
                                    for y in toReturn
                                ]
                            )
                if toSort == "roundNumber" and maxReturns <= offset + r["pageSize"]:
                    break
                if r["totalNumResults"] > offset + r["pageSize"]:
                    offset += r["pageSize"]
                else:
                    break
            print(values)
            if sortType == "avg":
                for k, v in values.items():
                    l = list(zip(*v))
                    values[k] = [[sum(x) / len(x) for x in l]]
            if sortType == "absolute":
                for key in values:
                    values[key] = sorted(
                        values[key], key=lambda x: x[index], reverse=isReversed
                    )
            print(values)
            values = sorted(
                values.items(),
                key=lambda item: (item[1][0][index], item[0]),
                reverse=isReversed,
            )
            print(values)
            count = 1
            for (key, val) in values:
                if count > maxReturns:
                    break
                s = []
                for line in val:
                    resultLine = [
                        (
                            str(line[i])
                            if args.raw
                            else utils.prettify(line[i], toReturn[i])
                        )
                        for i in range(len(toReturn))
                    ]
                    print(resultLine)
                    x = ""
                    if len(toReturn) == 1:
                        x = resultLine[0]
                    else:
                        x = resultLine[0] + ": " + ", ".join(resultLine[1:])
                    s.append(x)
                newline = "\n"
                if sortKey == toSort:
                    message += f"{newline.join(s)}\n"
                elif len(toReturn) == 1 or toSort == "roundNumber":
                    message += f"{key}: {', '.join(s)}\n"
                else:
                    message += f"{key}:\n{newline.join(s)}\n\n"
                count += 1
        print(message)

    else:
        # we are not using the api
        message = ""
        count = 0
        dataLines = utils.getAllData()
        isReversed = sortOrder == "max"
        if sortKey == "count":
            values = defaultdict(int)
            for line in dataLines:
                if not utils.inFilter(filterDict, line):
                    continue
                if line[toSort]:
                    values[line[toSort]] += 1
            values = sorted(
                values.items(), key=lambda item: (item[1], item[0]), reverse=isReversed
            )
            for (key, value) in values:
                if not (count < maxReturns and utils.inLimit(value, limit, isReversed)):
                    break
                if args.numbered:
                    message += f"{count+1}. "
                message += f"{key}: {value}\n"
                count += 1
        else:
            values = defaultdict(list)
            for line in dataLines:
                if not utils.inFilter(filterDict, line):
                    continue
                if line[sortKey]:
                    x = int(line[sortKey])
                    if x > 0 and (
                        utils.inLimit(x, limit, isReversed) or sortType == "avg"
                    ):
                        values[line[toSort]].append(
                            [
                                (x if y == sortKey else line.get(y, "?"))
                                for y in toReturn
                            ]
                        )
            if sortType == "avg":
                for k, v in values.items():
                    l = list(zip(*v))
                    values[k] = [[sum(x) / len(x) for x in l]]
            if sortType == "absolute":
                for key in values:
                    values[key] = sorted(
                        values[key], key=lambda x: x[index], reverse=isReversed
                    )
            values = sorted(
                values.items(),
                key=lambda item: (item[1][0][index], item[0]),
                reverse=isReversed,
            )
            for (key, val) in values:
                if count >= maxReturns and maxReturns > 0:
                    break
                s = []
                for li in val:
                    li = [
                        (str(li[i]) if args.raw else utils.prettify(li[i], toReturn[i]))
                        for i in range(len(li))
                    ]
                    x = ""
                    if len(toReturn) == 1:
                        x = li[0]
                    else:
                        x = li[0] + ": " + ", ".join(li[1:])
                    s.append(x)
                newline = "\n"
                if args.numbered:
                    message += f"{count+1}. "
                if sortKey == toSort:
                    message += f"{newline.join(s)}\n"
                else:
                    message += (
                        f"{key}: {', '.join(s)}\n"
                        if len(toReturn) == 1
                        else f"{key}:\n{newline.join(s)}\n\n"
                    )
                count += 1
    if len(message) > 2000:
        message = utils.pasteLongData(message)
    await ctx.send(message or "no matches")


@discord.command(pass_context=True)
@commands.is_owner()
async def update(ctx):
    "Updates the code. Only the owner can do that"
    reddit.saveData()
    await ctx.send("Restarting...")
    os.execl(sys.executable, sys.executable, *sys.argv)


@discord.command(pass_context=True)
@commands.is_owner()
async def fixNum(ctx):
    "Resets the internal round number to that of the current round"
    await reddit.fixNum()
    await ctx.send(f"Done. {reddit.currentNum}")


try:
    if config.updateReddit:
        discord.load_extension("subscriptions")
        discord.loop.create_task(reddit.run())
    discord.run(config.discord_token)
finally:
    print("Traceback in try/finally block:")
    traceback.print_exc()
