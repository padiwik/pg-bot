import csv
import datetime
import importlib
import sys
from collections import defaultdict
from datetime import datetime

import praw
import requests

import constants

config = importlib.import_module(sys.argv[1])


def processFilter(filterStr, default, index):  # index = 0 if old, 1 if api
    if not filterStr:
        return {}
    a = defaultdict(list)
    currentKey = default
    currentType = "in"
    currentCount = 0
    toAppend = []
    allowedWords = constants.allowedOperators
    for word in filterStr:
        if word in constants.keywords.keys():
            if len(toAppend) > 0:
                a[currentKey].append([currentType] + toAppend)
            currentKey = constants.keywords[word][index]
            currentType = "in"
            currentCount = 0
            toAppend = []
        else:
            if (
                currentType == "range"
                and currentCount == 2
                or allowedWords.get(word) in ["min", "max"]
                and currentCount == 1
                or word in allowedWords.keys()
            ):
                if len(toAppend) > 0:
                    a[currentKey].append([currentType] + toAppend)
                currentType = allowedWords.get(word) or "in"
                currentCount = 0
                toAppend = []
            else:
                if index == 1 and constants.keywords[currentKey][2] == "string":
                    # add quotes around arguments for the api query
                    word = "'" + word + "'"
                toAppend.append(word)
                currentCount += 1
    if len(toAppend) > 0:
        a[currentKey].append([currentType] + toAppend)
    print(a)
    return a


def createFilterString(filterDict):
    if not filterDict:
        print("No filter")
        return ""
    a = ""
    j = 0  # which key
    for key, val in filterDict.items():
        if j > 0:
            a += ") and "
        a += "("
        i = 0  # which list of this val
        for l in val:
            ftype = l[0]
            if i > 0:
                a += " or "
            if ftype == "in":
                a += "(" + " or ".join([key + " eq " + x for x in l[1:]]) + ")"
            if ftype == "min":
                a += key + " gte " + l[1]
            if ftype == "max":
                a += key + " lte " + l[1]
            if ftype == "range":
                a += key + " gte " + l[1] + " and " + key + " lte " + l[2]
            if ftype == "substring":
                a += key + " cont " + l[1]
            i += 1
        j += 1
    a += ")"
    print(a)
    return a


def inFilter(filterDict, line):
    for key, val in filterDict.items():
        if not line[key]:
            continue
        value = line[key]
        x = False
        for l in val:
            filterType = l[0]
            if filterType == "in":
                if value in l[1:]:
                    x = True
            if filterType == "min":
                if int(l[1]) <= int(value):
                    x = True
            if filterType == "max":
                if int(l[1]) >= int(value):
                    x = True
            if filterType == "range":
                if int(l[1]) <= int(value) <= int(l[2]):
                    x = True
            if filterType == "substring":
                if any([x in value.lower() for x in l[1:]]):
                    x = True
        if not x:
            return False
    return True


def getCorrectTime(n):
    data = getAllData()
    a = [x["cTimeStamp"] for x in data if int(x["number"]) == n]
    return int(a[0]) if len(a) > 0 else -1


def prettify(var, varName):
    if str(var) == "-1" or str(var) == "?":
        return "?"
    elif constants.keywords[varName][2] == "time":
        return humanTime(var)
    elif constants.keywords[varName][0] == "number":
        return f"round {var}"
    elif constants.keywords[varName][0] == "sId":
        # this is how we make a link to the round
        return f"<http://redd.it/{var}>"
    elif constants.keywords[varName][2] == "datetime":
        return humanDateTime(var)
    else:
        return str(var)


def getAttrOrInput(x, y, text):
    s = ""
    try:
        s = getattr(x, y)
    except:
        if config.askForInput:
            print("uhoh")
            s = input(f"{text} name: ")
        else:
            s = "?"
    return s


def getDiscordServer(discord):
    return discord.get_channel(config.chanID).guild


def writeData(submission, comment, number, prevTime, **kwargs):
    print(kwargs)
    answer = comment.parent()
    s = [
        number,
        submission.id,
        submission.title,
        kwargs.get("poster") or getAttrOrInput(submission.author, "name", "poster"),
        int(submission.created_utc),
        timeDif(submission.created_utc, prevTime) if prevTime > 0 else -1,
        answer.id,
        getattr(answer, "body", "[deleted]"),
        kwargs.get("winner") or getAttrOrInput(answer.author, "name", "solver"),
        int(answer.created_utc),
        timeDif(answer.created_utc, submission.created_utc),
        comment.id,
        getattr(comment.author, "name", "[deleted]"),
        int(comment.created_utc),
        timeDif(comment.created_utc, answer.created_utc),
    ]
    # this code does not handle [deleted] comments separately
    d = dict(zip(constants.keys, s))
    x = []
    for line in getAllData():
        if int(line["number"]) == int(number):
            print("line found!")
        else:
            x.append(line)
    x.append(d)
    writeLines(x)
    return d


def writeLines(s):
    # input is a list of dicts
    with open(config.dataFile, "r+") as csvFile:
        writer = csv.DictWriter(csvFile, constants.keys)
        csvFile.truncate(0)
        writer.writeheader()
        writer.writerows(s)
        csvFile.close()


def getAllData():
    with open(config.dataFile, newline="") as csvFile:
        return list(csv.DictReader(x.replace("\0", "") for x in csvFile))


def getKey(line, key, isRaw):
    x = line.get(key, "?")
    if not isRaw:
        x = prettify(x, key)
    return x


def inLimit(value, limit, isReversed):
    if limit == 0:
        return True
    return value <= limit and not isReversed or value >= limit and isReversed


# Choose a message that is short enough to send to discord
def chooseDiscordMessage(*args):
    # Return the first input that is at most 2000 characters
    for msg in args:
        if len(msg) <= 2000:
            return msg
    # If none are valid, just pastebin the first possible message
    return pasteLongData(args[0])


def pasteLongData(text):
    r = requests.post("https://pastie.io/documents", data=text.encode("utf-8"))
    return f"https://pastie.io/raw/{r.json()['key']}"


def timeDif(time1, time2):
    return int(
        (
            datetime.utcfromtimestamp(time1) - datetime.utcfromtimestamp(time2)
        ).total_seconds()
    )


def humanTime(seconds):
    if isinstance(seconds, str) and not seconds.isdigit():
        return "?"
    seconds = int(seconds)
    hours = seconds // 3600
    minutes = seconds // 60 % 60
    seconds = seconds % 60
    if hours > 0:
        return f"{hours} h {minutes} m"
    elif minutes > 0:
        return f"{minutes} m {seconds} s"
    else:
        return f"{seconds} s"


def humanDateTime(timestamp):
    dtObj = datetime.fromtimestamp(timestamp)
    return str(dtObj)
