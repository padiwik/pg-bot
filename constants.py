from collections import defaultdict

# for reddit

bots = ["AutoModerator", "r-PictureGame", "Achievements-Bot"]
roundsList = "http://reddit.com/r/PictureGame/new"

API_BASE_URL = "https://api.picturega.me/"

# for subscriptions

shelfName = "shelf"

pingeesFormat = {
    "all": set(),
    "round sub": defaultdict(set),
    "round unsub": defaultdict(set),
    "user sub": defaultdict(set),
    "user unsub": defaultdict(set),
}

# for sorting

allowedOperators = {
    "substring": "substring",
    "cont": "substring",
    "eq": "in",
    "in": "in",
    "range": "range",
    "min": "min",
    "gte": "min",
    "max": "max",
    "lte": "max",
}

keys = [
    "number",
    "sId",
    "sTitle",
    "sAuthor",
    "sTimeStamp",
    "sTimeDif",
    "aId",
    "aText",
    "aAuthor",
    "aTimeStamp",
    "aTimeDif",
    "cId",
    "cAuthor",
    "cTimeStamp",
    "cTimeDif",
]
keywords = {
    "number": ["number", "roundNumber", "number"],
    "roundNumber": ["number", "roundNumber", "number"],
    "round": ["number", "roundNumber", "number"],
    "sId": ["sId", "id", "string"],
    "id": ["sId", "id", "string"],
    "link": ["sId", "id", "string"],
    "sTitle": ["sTitle", "title", "string"],
    "title": ["sTitle", "title", "string"],
    "sAuthor": ["sAuthor", "hostName", "string"],
    "author": ["sAuthor", "hostName", "string"],
    "host": ["sAuthor", "hostName", "string"],
    "hostName": ["sAuthor", "hostName", "string"],
    "sTimeStamp": ["sTimeStamp", "postTime", "datetime"],
    "postTime": ["sTimeStamp", "postTime", "datetime"],
    "sTimeDif": ["sTimeDif", "postDelay", "time"],
    "postDelay": ["sTimeDif", "postDelay", "time"],
    "aId": ["aId", "winningCommentId", "string"],
    "winningCommentId": ["aId", "winningCommentId", "string"],
    "aText": ["aText", None, "string"],
    "answer": ["aText", None, "string"],
    "aAuthor": ["aAuthor", "winnerName", "string"],
    "winner": ["aAuthor", "winnerName", "string"],
    "winnerName": ["aAuthor", "winnerName", "string"],
    "aTimeStamp": ["aTimeStamp", "winTime", "datetime"],
    "winTime": ["aTimeStamp", "winTime", "datetime"],
    "aTimeDif": ["aTimeDif", "solveTime", "time"],
    "solveTime": ["aTimeDif", "solveTime", "time"],
    "winDelay": ["aTimeDif", "solveTime", "time"],
    "cId": ["cId", None, "string"],
    "cAuthor": ["cAuthor", None, "string"],
    "cTimeStamp": ["cTimeStamp", "plusCorrectTime", "datetime"],
    "correctTime": ["cTimeStamp", "plusCorrectTime", "datetime"],
    "plusCorrectTime": ["cTimeStamp", "plusCorrectTime", "datetime"],
    "cTimeDif": ["cTimeDif", "plusCorrectDelay", "time"],
    "correctDelay": ["cTimeDif", "plusCorrectDelay", "time"],
    "plusCorrectDelay": ["cTimeDif", "plusCorrectDelay", "time"],
}
