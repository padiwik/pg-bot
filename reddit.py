# This file holds the code that interacts with the PictureGame subreddit

import asyncio
import datetime
import importlib
import shelve
import sys
import traceback

import praw

import constants
import utils
import subscriptions

config = importlib.import_module(sys.argv[1])


class RedditParser:

    # This is a class variable as a hack for now to allow subscribing by round number
    currentNum = -1

    # We pass discord so that we can send messages to discord channels
    def __init__(self, discord):
        self.reddit = praw.Reddit(
            client_id=config.reddit_id,
            client_secret=config.reddit_secret,
            user_agent=config.description,
            username=config.reddit_username,
            password=config.reddit_pwd,
        )

        self.discord = discord

        self.subreddit = self.reddit.subreddit("PictureGame")
        self.mods = self.subreddit.moderator()
        self.bots = constants.bots

        with shelve.open(constants.shelfName) as shelf:
            if not "currentRound" in shelf:
                shelf["currentRound"] = None
            if not "seenComments" in shelf:
                shelf["seenComments"] = set()
            if not "hints" in shelf:
                shelf["hints"] = {}
            if not "correctTimeStamp" in shelf:
                shelf["correctTimeStamp"] = -1
            self.currentRound = shelf["currentRound"]
            self.seenComments = shelf["seenComments"]
            self.hints = shelf["hints"]
            self.correctTimeStamp = shelf[
                "correctTimeStamp"
            ]  # of the round before, to compute postDelay

        self.currentSolved = True  # whether the current round is solved

    async def run(self):
        await self.discord.wait_until_ready()
        self.chan = self.discord.get_channel(
            config.chanID
        )  # the main channel to send messages to
        self.guessChannel = self.discord.get_channel(
            config.guessChanID
        )  # the channel to send guesses and guess replies to

        while True:
            try:
                await self.redditLoop()
            except Exception as e:
                print(e)
                traceback.print_exc()
                try:
                    appInfo = await self.discord.application_info()
                    owner = appInfo.owner
                    await owner.send(
                        f"An error occurred - {e}. I may be slow or spammy."
                    )
                except Exception as e2:
                    print("Discord hates me :( - " + str(e2))
            await asyncio.sleep(6)

    async def redditLoop(self):
        if not self.currentSolved:
            # refresh the round comments
            self.currentRound = self.reddit.submission(id=self.currentRound.id)
            if self.currentRound.link_flair_text == "ABANDONED":
                await self.chan.send("Current round was abandoned")
                RedditParser.currentNum += 1
                self.currentSolved = True
                # TODO write (partial) data
            self.currentRound.comment_sort = "old"
            currentComments = self.currentRound.comments
            currentComments.replace_more(limit=None)
            comments = [
                comment
                for comment in currentComments.list()
                if comment.id not in self.seenComments
            ]
            comments.sort(key=lambda comment: comment.created_utc)
            if self.currentRound.author:
                for comment in comments:
                    await self.parseComment(comment)
        for submission in self.subreddit.new(limit=3):
            if (
                submission.id != self.currentRound.id
                and self.isValidTitle(submission.title)
                and (  # submission.created_utc > self.correctTimeStamp
                    not self.currentRound.created_utc
                    or submission.created_utc > self.currentRound.created_utc
                )
                and submission.link_flair_text in ["ROUND OVER", "UNSOLVED"]
            ):
                # We have a new round
                self.currentRound = submission
                self.currentSolved = False
                postNum = self.roundNum(self.currentRound)
                if self.correctTimeStamp < 0:
                    self.correctTimeStamp = utils.getCorrectTime(postNum - 1)
                self.seenComments = set()
                self.hints = {}
                if RedditParser.currentNum == -1:
                    RedditParser.currentNum = postNum
                await self.guessChannel.send(
                    f"New round by {self.currentRound.author}: **{self.currentRound.title}** <{self.currentRound.shortlink}>"
                )
                if RedditParser.currentNum != postNum:
                    print(f"WARNING: Internal number of {RedditParser.currentNum} does not match post number {postNum}. Please investigate")
                RedditParser.currentNum = postNum
                if config.announceRounds:
                    *_, roundPingees, titlePingees = subscriptions.getSubscriptionsPingees()
                    mentionsSuffix = subscriptions.mentionsSuffix(
                        self.discord,
                        roundPingees,
                        self.currentRound.author.name,
                        RedditParser.currentNum,  # We safely assume the number matches because pings only happen when it is correct
                        substringMentionsDict=titlePingees,
                        stringToSearch=self.currentRound.title
                    )
                    if RedditParser.currentNum != postNum:
                        await self.chan.send(
                            f"New post by {self.currentRound.author} that took {utils.humanTime(utils.timeDif(self.currentRound.created_utc,self.correctTimeStamp))} to post. However, the round number should be {RedditParser.currentNum} instead of {postNum}.{mentionsSuffix}\n{self.discordText(self.currentRound)}"
                        )
                    else:
                        await self.chan.send(
                            f"New round by {self.currentRound.author} that took {utils.humanTime(utils.timeDif(self.currentRound.created_utc,self.correctTimeStamp))} to post:{mentionsSuffix}\n{self.discordText(self.currentRound)}"
                        )
                print(self.currentRound.title)
                print(postNum)
                break
            elif (
                submission.id == self.currentRound.id and RedditParser.currentNum == -1
            ):
                self.currentSolved = False
                RedditParser.currentNum = self.roundNum(self.currentRound)
                print(
                    "Populated current round data from shelf. Round number",
                    RedditParser.currentNum,
                )
                break

    async def parseComment(self, comment):
        guessPingees, hintPingees, replyPingees, winPingees, *_ = subscriptions.getSubscriptionsPingees()
        if not comment.author:
            return
        if self.hints.get(comment.id):
            if comment.body != self.hints[comment.id]:
                # This hint has been edited
                mentionsSuffix = subscriptions.mentionsSuffix(
                    self.discord,
                    hintPingees,
                    self.currentRound.author.name,
                    RedditParser.currentNum,
                    excludeName=comment.author.name,
                )
                timeDif = self.printCurrentTime()
                # Check if the edited hint is just a new hint appended to the original one
                if comment.body.startswith(self.hints[comment.id]):
                    await self.chan.send(
                        utils.chooseDiscordMessage(
                            f"A hint was added (~{timeDif}): {comment.body[len(self.hints[comment.id]):]}{mentionsSuffix}",
                            f"A hint was added that is too long to post here (~{timeDif}).{mentionsSuffix}",
                        )
                    )
                else:
                    await self.chan.send(
                        utils.chooseDiscordMessage(
                            f"A hint was updated (~{timeDif}): {comment.body}{mentionsSuffix}",
                            f"A hint was updated that is too long to post here (~{timeDif}).{mentionsSuffix}",
                        )
                    )
                self.hints[comment.id] = comment.body
            return  # treat it as seen for other purposes
        if self.isValidGuessComment(
            comment, self.bots + [self.currentRound.author.name]
        ):
            guessMentionsSuffix = subscriptions.mentionsSuffix(
                self.discord,
                guessPingees,
                self.currentRound.author.name,
                RedditParser.currentNum,
                excludeName=comment.author.name,
            )
            await self.guessChannel.send(
                utils.chooseDiscordMessage(
                    f"{comment.author} guessed '{comment.body}'{guessMentionsSuffix}",
                    f"{comment.author} posted a guess too long to write here.{guessMentionsSuffix}",
                )
            )
        if self.isValidOpComment(
            comment, self.currentRound.author, self.mods, self.bots
        ):
            if self.isValidCorrectorComment(
                comment, self.bots + [self.currentRound.author.name]
            ):
                roundAttributes = utils.writeData(
                    self.currentRound,
                    comment,
                    RedditParser.currentNum,
                    self.correctTimeStamp,
                )
                print(comment.body)
                self.currentSolved = True
                self.correctTimeStamp = roundAttributes["cTimeStamp"]
                self.corrector = roundAttributes["cAuthor"]
                self.winner = roundAttributes["aAuthor"]
                winDelay = roundAttributes["aTimeDif"]
                answer = roundAttributes["aText"]
                winMentionsSuffix = subscriptions.mentionsSuffix(
                    self.discord, winPingees, self.winner, RedditParser.currentNum
                )
                answerMessagePrefix = f"The current round was solved by {self.winner} in {utils.humanTime(winDelay)}! "
                await self.chan.send(
                    utils.chooseDiscordMessage(
                        f"{answerMessagePrefix}The correct answer was {answer}{winMentionsSuffix}",
                        f"{answerMessagePrefix}The correct answer is too long to write here.{winMentionsSuffix}",
                    )
                )
                RedditParser.currentNum += 1
            elif self.isValidHintComment(comment, self.currentRound.author):
                hintMentionsSuffix = subscriptions.mentionsSuffix(
                    self.discord,
                    hintPingees,
                    self.currentRound.author.name,
                    RedditParser.currentNum,
                    excludeName=comment.author.name,
                )
                hintHumanTime = utils.humanTime(
                    utils.timeDif(comment.created_utc, self.currentRound.created_utc)
                )
                await self.chan.send(
                    utils.chooseDiscordMessage(
                        f"A hint was posted ({hintHumanTime}): {comment.body}{hintMentionsSuffix}",
                        f"A hint was posted that is too long to write here ({hintHumanTime}).{hintMentionsSuffix}",
                    )
                )
                self.hints[comment.id] = comment.body
                return
            elif comment.parent_id == comment.link_id and len(comment.body) <= 10:
                # this is a short reply to thread, which doesn't count as a hint
                replyMentionsSuffix = subscriptions.mentionsSuffix(
                    self.discord,
                    replyPingees,
                    "all",
                    RedditParser.currentNum,
                    excludeName=comment.author.name,
                )
                await self.guessChannel.send(
                    utils.chooseDiscordMessage(
                        f"OP replied to the thread with '{comment.body}'{replyMentionsSuffix}"
                    )
                )
            elif comment.parent_id != comment.link_id:
                # this is not a parent comment
                if self.isValidGuessComment(
                    comment.parent(), self.bots + [self.currentRound.author.name]
                ):
                    parentAuthor = comment.parent().author.name
                    replyMentionsSuffix = subscriptions.mentionsSuffix(
                        self.discord,
                        replyPingees,
                        parentAuthor,
                        RedditParser.currentNum,
                        excludeName=comment.author.name,
                    )
                    if replyMentionsSuffix:
                        await self.guessChannel.send(
                            utils.chooseDiscordMessage(
                                f"OP replied '{comment.body}' to {parentAuthor}'s guess '{comment.parent().body}'{replyMentionsSuffix}",
                                f"OP replied '{comment.body}' to a long guess by {parentAuthor}.{replyMentionsSuffix}",
                                f"OP replied to a long guess by {parentAuthor} with a long reply.{replyMentionsSuffix}",
                            )
                        )
        self.seenComments.add(comment.id)

    def saveData(self):
        with shelve.open(constants.shelfName) as shelf:
            shelf["currentRound"] = self.currentRound
            shelf["seenComments"] = self.seenComments
            shelf["hints"] = self.hints
        shelf.close()

    async def fixNum(self):
        RedditParser.currentNum = self.roundNum(self.currentRound)

    def isValidOpComment(self, comment, poster, mods, bots):
        return comment.author not in bots and (
            comment.author == poster or comment.distinguished or comment.author in mods
        )

    def isValidGuessComment(self, comment, badNames):
        return comment.author and comment.author.name not in badNames

    def isValidCorrectorComment(self, comment, badNames):
        # this function does not check if the commentor is a valid OP
        return (
            "+correct" in comment.body
            and not comment.is_root
            and self.isValidGuessComment(comment.parent(), badNames)
        )

    def isValidHintComment(self, comment, op):
        # Check if it is a top-level comment by the OP
        if comment.link_id == comment.parent_id and comment.author == op:
            if len(comment.body) > 10:  # ignore short comments like "x to all"
                return True
        if any(text in comment.body.lower() for text in ["hint", "clue", "clarif"]):
            return True
        return False

    def isValidTitle(self, title):
        return title.lower().startswith("[round ")

    def roundNum(self, submission):
        # we don't check if the title is valid or not, because automod does that
        return int(submission.title.lower().split("round ")[1].split("]")[0])
        # return int(submission.title[7:12])

    def timeSince(self, time):
        dif = datetime.datetime.utcnow() - datetime.datetime.utcfromtimestamp(time)
        return dif.seconds

    def discordText(self, submission):
        return f"""**{submission.title}**
<{submission.shortlink}>
{submission.url}"""

    def printCurrentRound(self):
        return self.discordText(self.currentRound)

    # Returns the current time (time since posted or time since corrected) prettified
    def printCurrentTime(self):
        if self.currentSolved:
            timeDif = self.timeSince(self.correctTimeStamp)
        else:
            timeDif = self.timeSince(self.currentRound.created_utc)
        return utils.humanTime(timeDif)

    # Returns a prettified string of the current status of the subreddit
    def printCurrentPretty(self):
        if self.currentSolved:
            return f"{self.winner}'s answer was +corrected {self.printCurrentTime()} ago by {self.corrector}. Please wait for the next round."
        else:
            return f"Round by {self.currentRound.author} unsolved for {self.printCurrentTime()}: {self.printCurrentRound()}"
